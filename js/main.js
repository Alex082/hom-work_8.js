/*

1 Опишіть своїми словами що таке Document Object Model (DOM)
 
  Он представляет страницу, чтобы программисти могли изменять структуру,
  стиль и содержимое документа.

2 Яка різниця між властивостями HTML-елементів innerHTML та innerText?

  innerHTML - выводит в консоль содержимое тега вместе с тегом,
  innerText - выводит в консоль содержимое тега без тега и просто текстом.

3 Як можна звернутися до елемента сторінки за допомогою JS? 

  createElement, getElementById, getElementsByName, getElementsByTagName и т.д.

 Який спосіб кращий?

  я думаю:

  что всё зависит от проэкта, поставленых задач проэкта и так далее, 
  например; getElementById будет быстрее чем querySelector, но querySelector 
  будет более лучше чтобы найти элементы за более сложными CSS - селекторами.
 
*/

// все параграфы
const paragraphs = document.querySelectorAll("p");
paragraphs.forEach((paragraph) => {
  paragraph.style.backgroundColor = "#ff0000";
});

// елемент id, родительский, дочернии елементы и ноды
const optionsList = document.getElementById("optionsList");
console.log(optionsList);

console.log(optionsList.parentNode);

if (optionsList.hasChildNodes()) {
  optionsList.childNodes.forEach((childNode) => {
    console.log(childNode.nodeName, childNode.nodeType);
  });
}

// testParagraph устанавливаем
const testParagraph = document.querySelector("#testParagraph");
testParagraph.textContent = "This is a paragraph";

// елементы main-header
const mainHeader = document.querySelector(".main-header");
const headerChild = mainHeader.children;

for (let i = 0; i < headerChild.length; i++) {
  console.log(headerChild[i]);
  headerChild[i].classList.add("nav-item");
}

// елементы section-title
const sectionTitle = document.querySelectorAll(".section-title");
sectionTitle.forEach((title) => {
  title.classList.remove("section-title");
});
